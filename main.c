#include "includes/main.h"

int main(int argc, char *argv[]) {
    Data_t *data;
    data = init_data_p(argv[1]);

    calc_and_print(data);

    return 0;
}
