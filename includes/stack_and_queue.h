#ifndef HW9V2_STACK_AND_QUEUE_H
#define HW9V2_STACK_AND_QUEUE_H

#include "data.h"
#include <stdlib.h>
#include <stdio.h>

Data_t *init_data_p (char *file_name);

void eater_pop(Data_t *data_p);
void muffin_pop(Data_t *data_p);

char get_eater_type(Data_t *data);
char get_muffin_type(Data_t *data);

void eater_move(Data_t *data);

#endif
