#ifndef HW9V2_DATA_H
#define HW9V2_DATA_H

typedef struct Eater_t {
    char type;
    struct Eater_t *next_eater_p;
} Eater_t;

typedef struct Eaters_t {
    struct Eater_t *first_eater_p, *last_eater_p;
    unsigned int moves_num, eaters_num;
} Eaters_t;

typedef struct Muffin_t {
    char type;
    struct Muffin_t *next_muffin_p;
} Muffin_t;

typedef struct Muffins_t {
    struct Muffin_t *first_muffin_p;
} Muffins_t;

typedef struct Data_t {
    struct Eaters_t *eaters;
    struct Muffins_t *muffins;
} Data_t;

#endif
