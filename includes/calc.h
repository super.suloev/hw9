
#ifndef HW9V2_CALC_H
#define HW9V2_CALC_H

#include "data.h"
#include "stack_and_queue.h"

void calc_and_print(Data_t *data);

#endif
