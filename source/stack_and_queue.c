#include "../includes/stack_and_queue.h"

Data_t *init_data_p (char *file_name) {
    Data_t *data_p;
    data_p = (Data_t*) malloc(sizeof(Data_t));

    Eaters_t *eaters_p;
    eaters_p = (Eaters_t*) malloc(sizeof(Eaters_t));
    eaters_p->eaters_num = 0;
    eaters_p->moves_num = 0;
    data_p->eaters = eaters_p;

    Muffins_t *muffins_p;
    muffins_p = (Muffins_t*) malloc(sizeof(Muffins_t));
    muffins_p->first_muffin_p = NULL;
    data_p->muffins = muffins_p;

    FILE *fp;
    fp = fopen(file_name, "r");
    char this_char = '/';
    while (this_char != EOF && this_char != '\n') {
        this_char = getc(fp);
        if (this_char == '0' || this_char == '1') {
            Eater_t *new_eater_p;
            new_eater_p = (Eater_t*) malloc(sizeof(Eater_t));
            new_eater_p->type = this_char;
            if (eaters_p->eaters_num++ == 0)
                eaters_p->first_eater_p = new_eater_p;
            else
                eaters_p->last_eater_p->next_eater_p = new_eater_p;
            new_eater_p->next_eater_p = NULL;
            eaters_p->last_eater_p = new_eater_p;
        }
    }

    this_char = '/';
    Muffin_t *last_muffin_p;
    while (this_char != EOF && this_char != '\n') {
        this_char = getc(fp);
        if (this_char == '0' || this_char == '1') {
            Muffin_t *new_muffin_p;
            new_muffin_p = (Muffin_t*) malloc(sizeof(Muffin_t));
            new_muffin_p->type = this_char;
            if (muffins_p->first_muffin_p == NULL) {
                muffins_p->first_muffin_p = new_muffin_p;
            }
            else {
                last_muffin_p->next_muffin_p = new_muffin_p;
            }
            new_muffin_p->next_muffin_p = NULL;
            last_muffin_p = new_muffin_p;
        }
    }


    fclose(fp);
    return data_p;
}


void eater_pop(Data_t *data_p) {
    Eaters_t *eaters_p;
    eaters_p = data_p->eaters;

    Eater_t *first_eater_p;
    first_eater_p = eaters_p->first_eater_p;

    eaters_p->first_eater_p = first_eater_p->next_eater_p;
    --eaters_p->eaters_num;
    eaters_p->moves_num = 0;
    free(first_eater_p);
}

void muffin_pop(Data_t *data_p) {
    Muffins_t *muffins_p;
    muffins_p = data_p->muffins;

    Muffin_t *first_muffin_p;
    first_muffin_p = muffins_p->first_muffin_p;

    muffins_p->first_muffin_p = first_muffin_p->next_muffin_p;
    free(first_muffin_p);
}


char get_eater_type(Data_t *data) {
    return data->eaters->first_eater_p->type;
}

char get_muffin_type(Data_t *data) {
    return data->muffins->first_muffin_p->type;
}

void eater_move(Data_t *data) {
    Eaters_t *eaters_p;
    eaters_p = data->eaters;

    Eater_t *first_eater_p, *second_eater_p, *last_eater_p;
    first_eater_p = eaters_p->first_eater_p;
    second_eater_p = eaters_p->first_eater_p->next_eater_p;
    last_eater_p = eaters_p->last_eater_p;

    eaters_p->first_eater_p = second_eater_p;
    eaters_p->last_eater_p = first_eater_p;
    first_eater_p->next_eater_p = NULL;
    last_eater_p->next_eater_p = first_eater_p;
    ++eaters_p->moves_num;
}