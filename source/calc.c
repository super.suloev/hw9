#include "../includes/calc.h"

void calc_and_print(Data_t *data) {
    Eaters_t *eaters_p;
    eaters_p = data->eaters;
    Muffins_t *muffins_t;
    muffins_t = data->muffins;

    while (eaters_p->moves_num < eaters_p->eaters_num) {
        if (get_eater_type(data) == get_muffin_type(data)) {
            eater_pop(data);
            muffin_pop(data);
        }
        else
            eater_move(data);
    }

    printf("%u", eaters_p->eaters_num);
}